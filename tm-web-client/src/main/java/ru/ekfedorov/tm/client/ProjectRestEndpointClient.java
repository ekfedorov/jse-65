package ru.ekfedorov.tm.client;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.ekfedorov.tm.api.IProjectRestEndpoint;
import ru.ekfedorov.tm.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectRestEndpointClient implements IProjectRestEndpoint {

    private final String address = "http://localhost:8080/api/projects";

    @Override
    public List<Project> findAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/findAll";
        final Project[] result = restTemplate.getForObject(url, Project[].class);
        return Arrays.asList(result);
    }

    @Override
    public Project find(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/find/{id}";
        final Project result = restTemplate.getForObject(url, Project.class, id);
        return result;
    }

    @Override
    public Project create(Project project) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/create";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(project, headers);
        final Project result = restTemplate.postForObject(url, entity, Project.class);
        return result;
    }

    @Override
    public List<Project> createAll(List<Project> projects) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/createAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(projects, headers);
        final Project[] result = restTemplate.postForObject(url, entity, Project[].class);
        return Arrays.asList(result);
    }

    @Override
    public Project save(Project project) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/save";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(project, headers);
        final Project result = restTemplate.postForObject(url, entity, Project.class);
        return result;
    }

    @Override
    public List<Project> saveAll(List<Project> projects) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/saveAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(projects, headers);
        final Project[] result = restTemplate.postForObject(url, entity, Project[].class);
        return Arrays.asList(result);
    }

    @Override
    public void delete(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/delete/{id}";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(headers);
        restTemplate.postForObject(url, entity, Project.class, id);
    }

    @Override
    public void deleteAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/deleteAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(headers);
        restTemplate.postForObject(url, entity, Project.class);
    }

}
