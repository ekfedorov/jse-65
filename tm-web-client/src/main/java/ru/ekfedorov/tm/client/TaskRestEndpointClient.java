package ru.ekfedorov.tm.client;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.ekfedorov.tm.api.ITaskRestEndpoint;
import ru.ekfedorov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointClient implements ITaskRestEndpoint {

    private final String address = "http://localhost:8080/api/tasks";

    @Override
    public List<Task> findAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/findAll";
        final Task[] result = restTemplate.getForObject(url, Task[].class);
        return Arrays.asList(result);
    }

    @Override
    public Task find(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/find/{id}";
        final Task result = restTemplate.getForObject(url, Task.class, id);
        return result;
    }

    @Override
    public Task create(Task task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/create";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(task, headers);
        final Task result = restTemplate.postForObject(url, entity, Task.class);
        return result;
    }

    @Override
    public List<Task> createAll(List<Task> tasks) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/createAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(tasks, headers);
        final Task[] result = restTemplate.postForObject(url, entity, Task[].class);
        return Arrays.asList(result);
    }

    @Override
    public Task save(Task task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/save";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(task, headers);
        final Task result = restTemplate.postForObject(url, entity, Task.class);
        return result;
    }

    @Override
    public List<Task> saveAll(List<Task> tasks) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/saveAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(tasks, headers);
        final Task[] result = restTemplate.postForObject(url, entity, Task[].class);
        return Arrays.asList(result);
    }

    @Override
    public void delete(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/delete/{id}";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(headers);
        restTemplate.postForObject(url, entity, Task.class, id);
    }

    @Override
    public void deleteAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = address + "/deleteAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        final HttpEntity entity = new HttpEntity<>(headers);
        restTemplate.postForObject(url, entity, Task.class);
    }

}
