package ru.ekfedorov.tm.endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ekfedorov.tm.api.IProjectRestEndpoint;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody List<Project> projects) {
        repository.saveAll(projects);
        return projects;
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/saveAll")
    public List<Project> saveAll(@RequestBody List<Project> projects) {
        repository.saveAll(projects);
        return projects;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        repository.removeAll();
    }

}
