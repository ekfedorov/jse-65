package ru.ekfedorov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/task/create")
    public String create() {
        taskRepository.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "task-edit",
                "command", taskRepository.findById(id)
        );
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task
    ) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "TASK EDIT";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

}
