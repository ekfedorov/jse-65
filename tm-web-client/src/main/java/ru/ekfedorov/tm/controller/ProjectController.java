package ru.ekfedorov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.repository.ProjectRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/project/create")
    public String create() {
        projectRepository.create();
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "project-edit",
                "command", projectRepository.findById(id)
        );
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project
    ) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "PROJECT EDIT";
    }

}
