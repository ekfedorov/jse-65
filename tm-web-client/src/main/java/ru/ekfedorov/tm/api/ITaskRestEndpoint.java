package ru.ekfedorov.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskRestEndpoint {

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody List<Task> tasks);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody List<Task> tasks);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}
