package ru.ekfedorov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.listener.AbstractListener;
import ru.ekfedorov.tm.event.ConsoleEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    private static final int INTERVAL = 3;

    @NotNull
    private static final String PATH = "./";

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public void init() {
        for (@NotNull final AbstractListener command : listeners) {
            if (command.commandArg() != null) commands.add(command.commandName());
        }
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull final File file = new File(PATH);
        for (File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean chek = commands.contains(fileName);
            if (!chek) continue;
            publisher.publishEvent(new ConsoleEvent(fileName));
            System.out.println();
            item.delete();
        }
    }

}
