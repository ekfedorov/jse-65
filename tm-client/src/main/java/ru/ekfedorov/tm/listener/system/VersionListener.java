package ru.ekfedorov.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String commandArg() {
        return "-v";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display program version.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @SneakyThrows
    @Override
    @EventListener(condition = "@versionListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
    }

}
