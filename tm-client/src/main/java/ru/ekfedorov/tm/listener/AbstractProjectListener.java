package ru.ekfedorov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ekfedorov.tm.endpoint.AdminEndpoint;
import ru.ekfedorov.tm.endpoint.Project;
import ru.ekfedorov.tm.endpoint.ProjectEndpoint;
import ru.ekfedorov.tm.exception.system.NullProjectException;

public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

    protected void showProject(@Nullable final Project project) throws Exception {
        if (project == null) throw new NullProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().value());
    }

}
