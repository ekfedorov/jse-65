package ru.ekfedorov.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractTaskListener;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all task.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskListListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (isEmpty(sort)) tasks = taskEndpoint.findAllTask(session);
        else tasks = taskEndpoint.findTaskAllWithComparator(session, sort);
        if (tasks.isEmpty()) {
            System.out.println("--- There are no tasks ---");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
        System.out.println();
    }

    @NotNull
    public String toString(@NotNull final Task task) {
        return task.getId() + ": " + task.getName() + " | " + task.getStatus();
    }

}
