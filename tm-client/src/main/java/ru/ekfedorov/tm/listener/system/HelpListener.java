package ru.ekfedorov.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;

@Component
public final class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String commandArg() {
        return "-h";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display list of terminal commands.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@helpListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener);
        }
    }

}
