package ru.ekfedorov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;

@Component
public final class ExitListener extends AbstractListener {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Close application.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    @EventListener(condition = "@exitListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        publisher.publishEvent(new ConsoleEvent("logout"));
        System.exit(0);
    }

}
