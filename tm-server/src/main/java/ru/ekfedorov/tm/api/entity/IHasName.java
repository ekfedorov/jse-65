package ru.ekfedorov.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

    void setName(@NotNull String name);

}
