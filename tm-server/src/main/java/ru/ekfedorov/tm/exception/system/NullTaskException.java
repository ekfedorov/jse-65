package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class NullTaskException extends AbstractException {

    public NullTaskException() {
        super("Error! TaskGraph is null...");
    }

}
